const gulp = require('gulp');
const sass = require('gulp-sass');
const postcss = require('gulp-postcss');
const cssDeclarationSorter = require('css-declaration-sorter');
const cssnano = require('gulp-cssnano');

sass.compiler = require('node-sass');

function style() {
    return gulp.src('./assets/scss/functions.scss')
        .pipe(sass({
            outputStyle: 'nested',
            precision: 3,
            errLogToConsole: true
        }))
        .pipe(postcss([
            cssDeclarationSorter({ order: 'smacss' })
        ]))
       
        .pipe(gulp.dest('./static/css'));
}

function watch() {
    gulp.watch('./assets/**/*.scss', style);
}

exports.style = style;
exports.watch = watch;