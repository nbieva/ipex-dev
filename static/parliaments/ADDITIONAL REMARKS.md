
# Meeting

+ **Wednesday in EP. Analyse with Oscar. Fully agree.  webstorm not working.** 
+ **Parliaments: worked with api as from wednesday. will finish today. still a bit on design to do on mobile phones.**
+ **Will NOT do all mockups**
+ **We do not have all chambers anyway. (Calin add albania this morning)**
+ **I have enough to work**
+ **Work on conferences : Goal is to raised specific cases we may encounter while implementaing these important pages.**
+ I am doing those 2 because I cannot wait to have real data
+ Starting on residences and after that I will correct and implement the updated layouts in  the Angular application. I would like to to finish those 2 in July. After ther will be the search + documents and scrutinies.
+ **Meeting with Chalee and Oscar**.
+ **Design search forms with Ant design components**? Oscar? Does it help?
+ Find a way to absorb IPEX contact points.
+ We should find way to **compress images**
+ We should find way to **slugify images names**
+ Are these two will be done on upload?
+  **Cortes Generales and Senado** ?
+ I will need, after, a deep meeting with oscar and Majdi on conferences and parliaments. we can already plan it.
+ Meeting with Tony with the team about testing workflow
+ Oscar Gallery manager dev > meeting for compress/resize/rename.

#### Conferences

+ How do I manage conferences?  have picture for the FI presidency (Cosac and Cfsp-cfdp for example..) But no logo no text. Others from HR presidency.
+ Conferences mostly images. No 

----

.fancybox-slide--image {
    overflow: hidden;
    padding: 5vw;
}

## IPEXL-1347 [RfC] / IPEX+ / Website BO and FO implementation Angular (CS001 ST01)

Develop or finalize the implementation of

- IPEX Board
- OWN section (New content section)
- COSAC content structure
- URL rewriting technical solution
- Browser Push notification
- Timeline and integration of Scrutiny events histogram
- Multilingual issues
- Mobile device compatibility
- HTML Print /notification/XML
- CELEX treasury browser tree
- GDPR compliance adaptation

## IPEXL-1348 [RfC] / IPEX+ / Related backend services (CS001 ST01)

Develop or finalize the implementation of

- REST CMS method
- Back-office for institutions
- Back-office for CMS
- OWN (new content) data entity model
- REST file manager module
- Browser notifications
- General Notifications
- My Stored Search (Admin & Result) Front Office
- Document classification with CELEX and EUROVOC for updating latest documents
- System administration dashboard
- REST CELEX module
- CELEX manager BO
- REST Eurovoc manager module
- Eurovoc manager BO

## IPEXL-1350 [RfC] / IPEX+ / Webservices to exchange information with IPEX (CS001 ST01)

Develop or finalize the implementation of webservices to retrieve information from IPEX for

- Documents, Dossiers and Scrutinies
- New content (OWN) Back-office for CMS
- Calender events

## To do??? IPEXL-1349 Rfc - [HomePage] On "DocumentsWithHighActivity" carousel display the number of labels for each document.

To be updated everywhere.
These are the main places where we find those badges:

+ Homepage: Important issues
+ Homepage: High activity documents
+ High activity landing page
+ Document: Header
+ Document: Scrutinies section, on each chamber
+ Scrutiny: Header (just the title)
+ Search results cards

## IPEXL-1351 [RfC] / IPEX+ / Test execution and updating the Master test document and plan (CS001 ST01)

+ Draft fiches for individual test cases for all functions in IPEX
+ Produce a comprehensive master test document, including a checklist
+ Elaborate a master test plan for IPEX
+ Include a dashboard of test results subdivided by main sections


# ADDITIONAL REMARKS

I have to do the same for conferences.
Parliamments finished 10 july. Conferences 31 july.

BANNERS: Backgrounds CSS do not accept white spaces in file names.
Carousels: bugs with autoplay.

+ public documents?
+ Logos SVG or PNG transparent
+ All images 1400 width or height
+ We should compress them and make a thumbnail version
+ Remove image from gallery when it is featured!
+ Maximum Contact points for EU matters? Possible to have Maximum 3? Makes sense?
+ WEBSITES BOTTOM PAGES??
+ Aren't we mixing "Parliament's websites" and"Webpages in English/National webpages"?

## ATNAT

+ Public documents?
+ URL should be separated in websites..

## BESEN

+ Parliament websites? I cannot find them.
+ I took the ones from ipex.eu
+ Legendes dans 4 langues. Comment gère-t-on?
+ Ajout des copyright manuellement. Prend du temps.
+ Banner description?
+ Problem with dutch and french social medias? (Facebook, Youtube..)

## BGNAR

+ Image descriptions are very long. Cannot be used as it is.
+ Summary was too long. Added in content then.
+ NARODNO SABRANIE en minuscules?
+ Réseaux sociaux?
+ 60.jpg has no description.

## CYVOU

+ No descriptions for images
+ No parliament websites? I added the one I found on the web
+ No social media?
+ Logo from JPG to PNG
+ Format text

## CZSEN

+ Je n'ai fait que l'anglais
+ Format logo from PS
+ Rename images
+ Resize images
+ No image descriptions
+ I inverted summary and "at a glance" in the long text.

## CZPOS

+ 8 EU contacts. Good example of a design to be reviewed when using real data.
+ Mr, Ms, Mrs
+ Rename images
+ Format logo from JPG
+ I inverted summary and "at a glance" in the long text.
+ No image descriptions

##DKFOL

+ formatting social media URLs
+ 885OPA.jpg + 8R2IBDAQ.jpg is really too small
+ No Contact point for EU matters ??

## Estonia (missing)

## FIEDU

All information about the parliamentary events of the Finnish EU Presidency (1 JUL – 31 DEC 2019) can be found on parleu2019.fi in Finnish, English and French. The site is no longer updated but will remain open until March 2021.


[Please note that the Eduskunta started automatic upload of XML files to the IPEX in September 2009. The database now contains dossiers as from the beginning of the parliamentary year of 2009. A link is created to the scrutiny page of the Eduskunta (in Finnish). The status of the dossier is updated only once (the option "completed" is not used, as in the Finnish system the dossier is active, in scrutiny, until the European act is adopted).]

## frass

+ all image sare too small !

## debta

+ Copyright dans les photos. Je l'ai retiré pour la bannière et la featured.
+ They ask for us to use actual IPEX materials from v2
+ problem avec legal regulations webpages dans DB
+ Why public docs on V2 and not on API interface??

## debra

No social media for germany

## huors

+ Hungary . Materials?

## iesea

+ Ireland. Materials?

## itsen

+ Italian senate

## itcam

+ Italian Chamber

## lvsae

+ Latvia: no summary?

## alkuv

+ PDF Documents to be downloadable? to add?


