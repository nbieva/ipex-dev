## History

After the Belgian Revolution of 1830, the National Congress decided on the Belgian Constitution and the state structure. Belgium would become a unitary state. A bicameral Parliament was chosen over a unicameral one. The Senate was seen as a more conservative and elite body that served as a counterweight to the more progressive Chamber of representatives. Senators were directly elected, but only those who paid ground taxes were eligible. Only men who paid taxes were allowed to vote. There was also the possibility for the heir to the throne to become senator by right.

In 1893 the principal of universal multiple voting right was introduced for men. This was combined with the principle of compulsory voting an the principle of proportional representation. For the Senate, candidates still needed to pay ground taxes, but the threshold was lowered. Moreover, a new category of provincial senators was introduced.

After World War I, the single vote system - "one man, one vote" - was introduced. This principle of universal suffrage was only applicable to men, women had no right to vote. They however could be a candidate in elections.

1921 saw a new reform for the Senate. The prerequisite of taxes was abolished and 4 categories of senators were created: directly elected senators, provincial senators, co-opted senators and senators by right.

After World War II, women finally received the same rights as men as far as voting was concerned.

The post-World War era saw a series of six Belgian state reforms from 1970 until 2014. In 1993, the competences of the Senate were redefined during the fourth State Reform. In 2014, during the sixth State Reform, the Senate transformed in the assembly it is today, with important reforms concerning composition and competences.

##Composition

Since the sixth state reform of 2014, the Senate consists of 60 members. 50 are elected by the parliaments of the federated entities, and 10 are co-opted members.

###Senators of the federated entities

Starting with the elections of 25 May 2014, 50 senators are appointed by and from the parliaments of the federated entities:

- 29 senators appointed by the Flemish Parliament from the Flemish Parliament or from the Dutch language group of the Parliament of the Brussels-Capital Region
-	10 senators appointed by and from the Parliament of the French Community (which itself is composed of all members of the Parliament of Wallonia and several members of the French language group of the Parliament of the Brussels-Capital Region)
-	8 senators appointed by and from the Parliament of Wallonia
-	2 senators appointed by and from the French-language group of the Parliament of the Brussels-Capital Region
-	1 senator appointed by and from the Parliament of the German-speaking Community

Whereas the German-speaking senator is chosen by plurality of votes in the Parliament of the German-speaking community, the seats of the senators of the federated entities are distributed proportionally between parties based on the results for the latest election for the federated entities, i.e. the Flemish Parliament, the Parliament of Wallonia and the Parliament of the Brussels-Capital Region (regional level).

###Co-opted senators

Ten senators are co-opted, meaning they are elected by their peers: six by the Dutch-language group of the Senate and four by the French-language group of the Senate. These seats are distributed proportionally between parties based on the results of the latest direct election of the Chamber of Representatives (federal level).

In 1893, the co-opted members were included in the Constitution as a new category of senators. It was intended to allow the senators to elect a number of experts or members of representative organizations to join them to enhance the quality of debate and legislation; however, political parties sometimes use it as a means of rewarding loyal members that were not elected.

## Qualifications

Article 69 of the Belgian Constitution sets forth four qualifications for senators: each senator must be at least 18 years old, must possess the Belgian nationality, must have the full enjoyment of civil and political rights, and must be resident in Belgium. Originally, the minimum age to be elected senator was 40; this was reduced to 21 in 1993 and eventually to 18 in 2014. A senator can only enter into office after having taken the constitutional oath, in either of the three official languages in Belgium: Dutch, French or German. They may also choose to take the oath in more than one language. The oath is as follows: "I swear to observe the Constitution". (Dutch: Ik zweer de Grondwet na te leven, French: Je jure d'observer la Constitution, German: Ich schwöre, die Verfassung zu befolgen)

Certain offices are incompatible with the office of senator. A member of the Senate may not be a member of the Chamber of Representatives at the same time and representatives must give up their seats in the Chamber of Representatives in order to join the Senate.

Another important incompatibility is based on the separation of powers. A senator who is appointed as a minister ceases to sit in the Senate and is replaced for as long as he is a minister. However, if he resigns as a minister, he may return to the Senate, in accordance with Article 50 of the Belgian Constitution. A senator cannot also be a civil servant or a member of the judiciary at the same time. However, a civil servant who is elected to the Senate is entitled to political leave and doesn't have to resign as a civil servant. It is also not possible to be a member of the Federal Parliament and a Member of the European Parliament at the same time.

## Language groups

With the exception of the senator appointed by the Parliament of the German-speaking Community, all senators are divided into two language groups: a Dutch language group and a French language group. The Dutch language group consists of members appointed by the Flemish Parliament, members appointed by the Dutch language group of the Parliament of the Brussels-Capital Region, and the members co-opted by the two aforementioned groups. The French language group consists of members appointed by the Parliament of the French Community, members appointed by the Parliament of Wallonia, members appointed by the French language group of the Parliament of the Brussels-Capital Region, and the members co-opted by the three aforementioned groups. There are 35 senators in the Dutch language group and 24 in the French language group.

## Brussels

Article 67 of the Belgian Constitution determines that at least one of the senators in the Dutch language group must be resident in the Brussels-Capital Region on the date of their election, as well as six of the senators in the French language group.

## Gender

Since the sixth state reform, the Belgian Constitution stipulates that no more than two-thirds of senators are of the same sex.  Thus, the Senate is the only Belgian legislative body where a representation of each sex is guaranteed.

## Presidency

The Speaker of the Senate is elected by the Senate at the beginning of each parliamentary term. The Speaker is assisted by two Vice-Presidents, who are also elected at the beginning of each parliamentary term. The Speaker of the Senate is usually a member of a party of the majority with a great deal of political experience, while the First Vice-President is a member of the other language group.

The Speaker of the Senate presides over the plenary assembly of the Senate, manages and controls debates in the assembly, is responsible for ensuring the democratic functioning of the Senate, maintains order and security in the assembly and enforces the Rules of the Senate. He or she also represents the Senate at both the national (to the other institutions) and the international level. Additionally, he or she chairs the Bureau, which determines the order of business, supervises the administrative services of the Senate, and leads the Senate's activities.
The Speaker of the Senate, together with the President of the Chamber of Representatives, ranks immediately behind the King in the order of precedence. The elder of the two takes the second place in the order of precedence.

The Bureau of the Senate is composed of the Speaker, the two Vice-Presidents, and the leaders of the political groups that are represented in the standing committees. The Bureau leads the day-to-day activities of the Senate and convenes every two weeks in order to manage the work of the Senate. The Bureau determines the legislative agenda and the order of business in the plenary assembly and the committees, and manages the internal affairs of the Senate. A member of the Federal Government is usually invited to attend the discussions about the legislative agenda. The Bureau also assists the Speaker in the conduct of parliamentary business. In addition, the Bureau also appoints, promotes, and dismisses the staff of the Senate.

The Senate is served by a number of civil servants. The Senate's chief administrative officer is the Secretary-General of the Senate, who is appointed by the assembly and heads the Senate's legislative services. He is assisted by the Director-General who heads the administrative services and replaces the Secretary-General in his absence.

## Competences
The Senate mainly has the following tasks:

-	Legislation: The Senate, together with the Chamber, has full competence for the Constitution and legislation on the organization and functioning of the Federal State and the federated entities. Depending on the type of law, different procedures are possible that require, among others, special majorities (i.e. a two-third majority in the Senate, combined with a majority in each language group).
-	Information reports: The Senate may draw up information reports, in particular concerning topics that are governed by rules and regulations of different state levels.
-	Conflicts of interest: Within the logic of federalism, the Senate mediates in possible conflicts of interest between the different parliaments of the country.
-	International parliamentary organizations: The Senate sends parliamentary delegations to different interparliamentary conferences and organizations. Through their representatives in the Senate, the federated entities have access to this interparliamentary cooperation.
-	Subsidiarity: Following the EU Treaties, the Senate has the right as chamber of the federal parliament to ensure that the European Union does not take an initiative on an issue that is better dealt with at another level. This is the so-called early warning mechanism concerning subsidiarity.
-	Appointments to courts: The Senate participates in a number of appointments to high courts (Constitutional Court, Council of State, High Council of Justice).

## Committees

Before a dossier or topic for debate is dealt with in plenary, it is prepared in one of the committees of the Senate. In addition to the standing committees, the Senate can set up special committees, working groups, advisory committees or joint committees (together with the Chamber). A standing committee consists of 20 senators. The political strength of the committees reflects the strength of the political groups in the plenary session.

The Senate had three standing committees:

-	Institutional Affairs Committee: deals mainly with issues related tot the Constitution and the legislation on the organization and functioning of the Federal State and the federated entities.
-	Transversal Affairs Committee: deals mainly with issues that are governed by rules and regulations of different state levels.
-	Committee for Democratic Renewal and Citizenship.
The Senate also has an Equal Opportunities Advisory Committee that focuses on equality between women and men.
The Senate and the Chamber of representatives have established three joint committees:
-	Federal Advisory Committee on EU Affairs
-	Parliamentary Committee for Law Evaluation
-	Parliamentary Concertation Committee: this committee deals with all issues relating to the legislative processes where both the Senate and the Chamber are involved in.
