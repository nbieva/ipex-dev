const pkg = require("./package");

module.exports = {
    mode: "spa",
    head: {
        title: "IPEX | The platform for EU Interparliamentary Exchange",
        meta: [
            { charset: "utf-8" },
            { name: "viewport", content: "width=device-width, initial-scale=1" },
            { name: "robots", content: "noindex, nofollow" },
            { hid: "description", name: "description", content: pkg.description }
        ],
        link: [{ rel: "icon", type: "image/x-icon", href: "/favicon.ico" }],
        script: [{
                src: "/js/jquery.min.js"
            },
            {
                src: "https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
            },
            {
                src: "https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"
            },
            {
                src: "/js/bootstrap.min.js"
            },
            {
                src: "/js/popper.min.js"
            },
            {
                src: "/js/bootstrap-maxlength.js"
            },
            {
                src: "/js/bootstrap-datepicker.min.js"
            },
            {
                src: "/js/ipex.js"
            }
        ]
    },
    plugins: [
        //'@plugins/vuetify',
        '@plugins/ant-design-vue',
        {
            src: '~/plugins/vue-editor.js',
            ssr: false
        }
    ],
    loading: { color: "#15293F" },
    css: [
        '@/styles/less/bootstrap/bootstrap.css',
        '@/styles/less/ipex.less'
    ],

    modules: [
        '@nuxtjs/axios',
        ['vue-scrollto/nuxt', { duration: 300 }],
        '@nuxtjs/style-resources',
        '@nuxtjs/device',
        'nuxt-moment'
    ],
    styleResources: {
        less: [
            '@/styles/less/ipex.less'
        ]
    },
    router: {
        base: "/"
    },

    build: {
        extractCSS: true,
        extend(config, ctx) {},
        postcss: {
            plugins: {
                'autoprefixer': {},
                'css-declaration-sorter': {
                    order: 'smacss'
                },
                'cssnano': {
                    preset: 'default'
                },
            },
            preset: {
                autoprefixer: {
                    grid: true
                }
            }
        }
    }
}

